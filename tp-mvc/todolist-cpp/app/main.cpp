#include <todolist-cpp/todolist-cpp.hpp>

#include "../include/todolist-cpp/View.hpp"
#include "../include/todolist-cpp/Board.hpp"

#include <iostream>

int main() {
    Board b;
    View v;
    b.add("tache1");
    b.add("tache3");
    b.add("tache2");
    b.Done("tache1");
    b.Done("tache2");
    v.printBoard(b);
}

