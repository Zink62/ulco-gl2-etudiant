#pragma once



#include <fstream>
#include <memory>
#include <vector>
#include <iostream>
#include <list>
#include <algorithm>

class Board {
    private:
        std::list<std::string> _taskTodo;
        std::list<std::string> _taskDone;
        int _taskId;


    public:
        Board() {}

        void add(const std::string & t) {
            _taskTodo.push_back(t);
            _taskId++;
        }

        void Done(const std::string & t) {
            _taskDone.push_back(t);
            _taskTodo.remove(t);
        }

        std::list<std::string> getTodo(){
            return _taskTodo;
        }

        std::list<std::string> getDone(){
            return _taskDone;
        }
};
