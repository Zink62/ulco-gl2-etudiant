#pragma once

#include "Board.hpp"

#include <fstream>
#include <memory>
#include <vector>
#include <iostream>
#include <list>
#include <algorithm>

class View {
    public:
        View() {}

        void printBoard(Board & b){
            std::cout << "TODO"  << std::endl;
            for(std::string n: b.getTodo())
            {
                std::cout << n << std::endl;
            }
            
            
            std::cout << "DONE"   << std::endl;

            for(std::string n: b.getDone())
            {
                std::cout << n << std::endl;
            }
        }

};