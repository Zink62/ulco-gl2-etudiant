import Board
import View

-- import Text.Read (readMaybe)

loop :: Board -> IO ()
loop b = do
    putStrLn ""
    printBoard b

    a <- getLine
    case words a of
        ["quit"] -> putStr "Bye"
        ("add":xs) -> 
            let 
                (i, b2) = addTodo (unwords xs) b
            in loop b2
        ["done",x] -> loop $  toDone (read x) b 
        _ -> loop b
    -- TODO

main :: IO ()
main = do
    loop newBoard

