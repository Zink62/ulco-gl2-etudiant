
#include "Board.hpp"
#include "NumBoard.hpp"
#include "Reportable.hpp"
#include "ReportFile.hpp"
#include "ReportStdout.hpp"

void testBoard(Board & b, Reportable &report) {
    std::cout << b.getTitle() << std::endl;
    b.add("item 1");
    b.add("item 2");

    report.report(b);
}

void testNumBoard(NumBoard & b, Reportable &report) {
    std::cout << b.getTitle() << std::endl;
    b.addNum("item 1");
    b.addNum("item 2");

    report.report(b);
}

int main() {

    Board b1;
    ReportFile r("test.txt");
    testBoard(b1,r);

    NumBoard b2;
    testNumBoard(b2,r);

    return 0;
}

