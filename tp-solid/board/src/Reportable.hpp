#pragma once

class Reportable{
    public:
        virtual void report(const Itemable & itemable) = 0;
};