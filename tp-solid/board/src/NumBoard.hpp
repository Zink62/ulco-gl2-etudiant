#pragma once

class NumBoard : public Board{
    private:
        int nextId;
    public:
    NumBoard(): Board(), nextId(1){};
    void addNum(const std::string &t){
        std::string tmp = std::to_string(nextId) + ". "+t;
        add(tmp);
        nextId++;
    };
};