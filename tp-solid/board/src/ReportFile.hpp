#pragma once

#include "Itemable.hpp"
#include "Reportable.hpp"

#include <fstream>
#include <iostream>

class ReportFile :public Reportable {
    private:

        std::ofstream _ofs;

    public:
        ReportFile(std::string filename): _ofs(filename){}
        void report(const Itemable & itemable) override {
            for (const std::string & item : itemable.getItems())
                _ofs << item << std::endl;
            _ofs << std::endl;
        }
};