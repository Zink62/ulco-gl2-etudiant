#pragma once

#include "Board.hpp"
#include "Reportable.hpp"

#include <fstream>
#include <iostream>

class ReportStdout: public Reportable  {
    public:
        void report(const Itemable & itemable) override {
            for (const std::string & item : itemable.getItems())
                std::cout << item << std::endl;
            std::cout << std::endl;
        }

};