#include <math.h>

double sinus(int n){
    double result;
    result = sin(n);
    return result;
}

#include <emscripten/bind.h>

EMSCRIPTEN_BINDINGS(sinus) {
    emscripten::function("sinus", &sinus);
}
