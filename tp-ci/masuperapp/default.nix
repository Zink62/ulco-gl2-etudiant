with import <nixpkgs> {};

let
  masuperlib-src = fetchTarball "https://gitlab.com/Zink62/remi-fouquerolle-masuperlib/-/archive/v0.1/masuperlib-v0.1.tar.gz";
  masuperlib = callPackage masuperlib-src {};

in clangStdenv.mkDerivation {
    name = "masuperpp";
    src = ./.;
    nativeBuildInputs = [ cmake ];
    buildInputs = [ masuperlib ];
}

